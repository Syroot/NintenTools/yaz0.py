from setuptools import setup

setup(name="yaz0",
      packages=["yaz0"],
      version="0.1.0",
      python_requires=">=3",
      description="Python package for decompressing Nintendo Yaz0 data.",
      keywords="nintendo yaz0",
      author="Syroot",
      author_email="dev@syroot.com",
      license="MIT",
      url="https://github.com/Syroot/yaz0",
      download_url="https://github.com/Syroot/yaz0/archive/0.1.0.tar.gz")
