# yaz0
Python package for decompressing Nintendo Yaz0 data.

## Usage

Simply `import yaz0` and then pass your bytes-like object to `yaz0.decompress`.
It returns the raw, decompressed data as a bytearray.

## PyPI
The package can be found on [PyPI](https://pypi.python.org/pypi/yaz0).
